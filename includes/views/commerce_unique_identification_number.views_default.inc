<?php
/**
 * Default Views implementations
 * 
 * @package commerce_unique_identification_number
 * @author carstenharnisch
 */

/**
 * implements hooks views_default_views
 */
function commerce_unique_identification_number_views_default_views() {
	$view = new view();
	$view->name = 'commerce_unique_identification_number';
	$view->description = 'Displays UIN information';
	$view->tag = 'default, commerce';
	$view->base_table = 'commerce_unique_identification_number';
	$view->human_name = 'Commerce UIN';
	$view->core = 7;
	$view->api_version = '3.0';
	$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
	
	/* Display: Master */
	$handler = $view->new_display('default', 'Master', 'default');
	$handler->display->display_options['title'] = 'My UINs';
	$handler->display->display_options['use_ajax'] = TRUE;
	$handler->display->display_options['use_more_always'] = FALSE;
	$handler->display->display_options['access']['type'] = 'perm';
	$handler->display->display_options['access']['perm'] = 'administer uins';
	$handler->display->display_options['cache']['type'] = 'none';
	$handler->display->display_options['query']['type'] = 'views_query';
	$handler->display->display_options['exposed_form']['type'] = 'basic';
	$handler->display->display_options['pager']['type'] = 'full';
	$handler->display->display_options['pager']['options']['items_per_page'] = '10';
	$handler->display->display_options['style_plugin'] = 'table';
	$handler->display->display_options['style_options']['columns'] = array(
			'sid' => 'sid',
			'activated' => 'activated',
			'code' => 'code',
			'created' => 'created',
			'title' => 'title',
			'sku' => 'sku',
			'name' => 'name',
	);
	$handler->display->display_options['style_options']['default'] = '-1';
	$handler->display->display_options['style_options']['info'] = array(
			'sid' => array(
					'align' => '',
					'separator' => '',
					'empty_column' => 0,
			),
			'activated' => array(
					'sortable' => 0,
					'default_sort_order' => 'asc',
					'align' => '',
					'separator' => '',
					'empty_column' => 0,
			),
			'code' => array(
					'align' => '',
					'separator' => '',
					'empty_column' => 0,
			),
			'created' => array(
					'sortable' => 0,
					'default_sort_order' => 'asc',
					'align' => '',
					'separator' => '',
					'empty_column' => 0,
			),
			'title' => array(
					'sortable' => 0,
					'default_sort_order' => 'asc',
					'align' => '',
					'separator' => '',
					'empty_column' => 0,
			),
			'sku' => array(
					'sortable' => 0,
					'default_sort_order' => 'asc',
					'align' => '',
					'separator' => '',
					'empty_column' => 0,
			),
			'name' => array(
					'sortable' => 0,
					'default_sort_order' => 'asc',
					'align' => '',
					'separator' => '',
					'empty_column' => 0,
			),
	);
	/* Relationship: Commerce UIN: Product ID */
	$handler->display->display_options['relationships']['product_id']['id'] = 'product_id';
	$handler->display->display_options['relationships']['product_id']['table'] = 'commerce_unique_identification_number';
	$handler->display->display_options['relationships']['product_id']['field'] = 'product_id';
	/* Relationship: Commerce UIN: Creator */
	$handler->display->display_options['relationships']['uid']['id'] = 'uid';
	$handler->display->display_options['relationships']['uid']['table'] = 'commerce_unique_identification_number';
	$handler->display->display_options['relationships']['uid']['field'] = 'uid';
	/* Field: Commerce UIN: Sid */
	$handler->display->display_options['fields']['sid']['id'] = 'sid';
	$handler->display->display_options['fields']['sid']['table'] = 'commerce_unique_identification_number';
	$handler->display->display_options['fields']['sid']['field'] = 'sid';
	/* Field: Commerce UIN: Activated */
	$handler->display->display_options['fields']['activated']['id'] = 'activated';
	$handler->display->display_options['fields']['activated']['table'] = 'commerce_unique_identification_number';
	$handler->display->display_options['fields']['activated']['field'] = 'activated';
	$handler->display->display_options['fields']['activated']['type'] = 'unicode-yes-no';
	$handler->display->display_options['fields']['activated']['not'] = 0;
	/* Field: Commerce UIN: Code */
	$handler->display->display_options['fields']['code']['id'] = 'code';
	$handler->display->display_options['fields']['code']['table'] = 'commerce_unique_identification_number';
	$handler->display->display_options['fields']['code']['field'] = 'code';
	/* Field: Commerce UIN: Created */
	$handler->display->display_options['fields']['created']['id'] = 'created';
	$handler->display->display_options['fields']['created']['table'] = 'commerce_unique_identification_number';
	$handler->display->display_options['fields']['created']['field'] = 'created';
	$handler->display->display_options['fields']['created']['date_format'] = 'short';
	/* Field: Commerce Product: Title */
	$handler->display->display_options['fields']['title']['id'] = 'title';
	$handler->display->display_options['fields']['title']['table'] = 'commerce_product';
	$handler->display->display_options['fields']['title']['field'] = 'title';
	$handler->display->display_options['fields']['title']['relationship'] = 'product_id';
	$handler->display->display_options['fields']['title']['label'] = 'Product';
	$handler->display->display_options['fields']['title']['link_to_product'] = 0;
	/* Field: Commerce Product: SKU */
	$handler->display->display_options['fields']['sku']['id'] = 'sku';
	$handler->display->display_options['fields']['sku']['table'] = 'commerce_product';
	$handler->display->display_options['fields']['sku']['field'] = 'sku';
	$handler->display->display_options['fields']['sku']['relationship'] = 'product_id';
	$handler->display->display_options['fields']['sku']['link_to_product'] = 0;
	/* Field: User: Name */
	$handler->display->display_options['fields']['name']['id'] = 'name';
	$handler->display->display_options['fields']['name']['table'] = 'users';
	$handler->display->display_options['fields']['name']['field'] = 'name';
	$handler->display->display_options['fields']['name']['relationship'] = 'uid';
	$handler->display->display_options['fields']['name']['label'] = 'CreatorUser';
	
	/* Display: user specific */
	$handler = $view->new_display('page', 'user specific', 'page');
	$handler->display->display_options['defaults']['access'] = FALSE;
	$handler->display->display_options['access']['type'] = 'perm';
	$handler->display->display_options['access']['perm'] = 'view own uins';
	$handler->display->display_options['path'] = 'user/%/commerce-uin';
	$handler->display->display_options['menu']['type'] = 'tab';
	$handler->display->display_options['menu']['title'] = 'My UINs';
	$handler->display->display_options['menu']['weight'] = '0';
	$handler->display->display_options['menu']['context'] = 0;
	$handler->display->display_options['menu']['context_only_inline'] = 0;
	
	/* Display: admin specific */
	$handler = $view->new_display('page', 'admin specific', 'page_1');
	$handler->display->display_options['defaults']['title'] = FALSE;
	$handler->display->display_options['title'] = 'Admin UINs';
	$handler->display->display_options['path'] = 'admin/commerce/orders/uins';
	$handler->display->display_options['menu']['type'] = 'tab';
	$handler->display->display_options['menu']['title'] = 'Admin UIN';
	$handler->display->display_options['menu']['weight'] = '0';
	$handler->display->display_options['menu']['context'] = 1;
	$handler->display->display_options['menu']['context_only_inline'] = 0;
			
  	$views [$view->name] = $view;
  
	// At the end, return array of default views.
  	return $views;
}
?>
