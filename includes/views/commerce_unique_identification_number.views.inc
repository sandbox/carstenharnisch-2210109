<?php
/**
 * Implements Views
 *
 * @package Commerce
 * @author carstenharnisch
 */

/**
 * implements hook_views_data
 */
function commerce_unique_identification_number_views_data() {
	$data ['commerce_unique_identification_number'] = array (
			'table' => array (
					'base' => array (
							'field' => 'sid',
							'title' => 'Commerce Unique Identification Number',
							'help' => 'The base table for UIN.' 
					),
					'group' => 'Commerce UIN' 
			),
			'sid' => array (
					'title' => 'Sid',
					'help' => 'The primary identifier for a UIN, used internally only.',
					'field' => array (
							'handler' => 'views_handler_field_numeric',
							'click sortable' => FALSE 
					) 
			),
			'order_id' => array (
					'title' => t ( 'Order ID' ),
					'help' => t ( 'The unique internal identifier of the order.' ),
					'field' => array (
							'handler' => 'commerce_order_handler_field_order',
							'click sortable' => TRUE 
					),
					'filter' => array (
							'handler' => 'views_handler_filter_string' 
					),
					'sort' => array (
							'handler' => 'views_handler_sort' 
					),
					'argument' => array (
							'handler' => 'commerce_order_handler_argument_order_order_id',
							'name field' => 'order_label',
							'numeric' => TRUE,
							'validate type' => 'order_id' 
					),
					'relationship' => array (
							'handler' => 'views_handler_relationship',
							'base' => 'commerce_order',
							'field' => 'order_id',
							'label' => t ( 'Order', array (), array (
									'context' => 'a drupal commerce order' 
							) ) 
					) 
			),
			'product_id' => array (
					'title' => t ( 'Product ID' ),
					'help' => t ( 'The unique internal identifier of the product.' ),
					'field' => array (
							'handler' => 'commerce_product_handler_field_product',
							'click sortable' => TRUE
					),
					'filter' => array (
							'handler' => 'views_handler_filter_string'
					),
					'sort' => array (
							'handler' => 'views_handler_sort'
					),
					'argument' => array (
							'handler' => 'commerce_product_handler_argument_product_id',
							/*
							'name field' => 'title',
							'numeric' => TRUE,
							'validate type' => 'product_id' */
					),
					'relationship' => array (
							'handler' => 'views_handler_relationship',
							'base' => 'commerce_product',
							'field' => 'product_id',
							'label' => t ( 'Product', array (), array (
									'context' => 'a drupal commerce product'
							) )
					)
			),
			'key_owner_id' => array (
					'title' => t ( 'Key Owner' ),
					'help' => t ( "The owner's user ID." ),
					'field' => array (
							'handler' => 'views_handler_field_user',
							'click sortable' => TRUE 
					),
					'argument' => array (
							'handler' => 'views_handler_argument_user_uid',
							'name field' => 'name'  
										),
					'filter' => array (
							'title' => t ( 'Name' ),
							'handler' => 'views_handler_filter_user_name' 
					),
					'sort' => array (
							'handler' => 'views_handler_sort' 
					),
					'relationship' => array (
							'title' => t ( 'Owner' ),
							'help' => t ( "Relate this order to its owner's user account" ),
							'handler' => 'views_handler_relationship',
							'base' => 'users',
							'base field' => 'uid',
							'field' => 'uid',
							'label' => t ( 'Order owner' ) 
					) 
			), 
			'code' => array (
					'title' => 'Code',
					'help' => 'The unique, human-readable identifier for a UIN.',
					'field' => array (
							'handler' => 'views_handler_field',
							'click sortable' => FALSE 
					) 
			),
			'type' => array (
					'title' => 'Type',
					'help' => 'The commerce_product_key_type.type of this UIN. For future use.',
					'field' => array (
							'handler' => 'views_handler_field',
							'click sortable' => FALSE 
					) 
			),
			'activated' => array (
					'title' => t ( 'Activated' ),
					'help' => t ( 'Whether or not the UIN is activated.' ),
					'field' => array (
							'handler' => 'views_handler_field_boolean',
							'click sortable' => TRUE,
							'output formats' => array (
									'active-disabled' => array (
											t ( 'Active' ),
											t ( 'Unactivated' ) 
									) 
							) 
					),
					'filter' => array (
							'handler' => 'views_handler_filter_boolean_operator',
							'label' => t ( 'Active' ),
							'type' => 'yes-no' 
					),
					'sort' => array (
							'handler' => 'views_handler_sort' 
					) 
			),
			'revoked' => array (
					'title' => t ( 'Revoked' ),
					'help' => t ( 'Whether or not the UIN is revoked.' ),
					'field' => array (
							'handler' => 'views_handler_field_boolean',
							'click sortable' => TRUE,
							'output formats' => array (
									'active-disabled' => array (
											t ( 'Revoked' ),
											t ( 'Active' ) 
									) 
							) 
					),
					'filter' => array (
							'handler' => 'views_handler_filter_boolean_operator',
							'label' => t ( 'Active' ),
							'type' => 'yes-no' 
					),
					'sort' => array (
							'handler' => 'views_handler_sort' 
					) 
			),
			'language' => array (
					'title' => 'Language',
					'help' => 'The languages.language of this UIN.',
					'field' => array (
							'handler' => 'views_handler_field',
							'click sortable' => FALSE 
					) 
			),
			'uid' => array (
					'title' => t ( 'Creator' ),
					'help' => t ( 'Relate a UIN to the user who created it.' ),
					'relationship' => array (
							'handler' => 'views_handler_relationship',
							'base' => 'users',
							'field' => 'uid',
							'label' => t ( 'UIN creator' ) 
					) 
			),
			'status' => array (
					'title' => 'Status',
					'help' => 'Integer Code indicating whether or not the UIN appears in lists.',
					'field' => array (
							'handler' => 'views_handler_field_numeric',
							'click sortable' => FALSE 
					) 
			),
			'created' => array (
					'title' => 'Created',
					'help' => 'The date the UIN was created.',
					'field' => array (
							'handler' => 'views_handler_field_date',
							'click sortable' => TRUE 
					),
					'sort' => array (
							'handler' => 'views_handler_sort_date' 
					),
					'filter' => array (
							'handler' => 'views_handler_filter_date' 
					) 
			),
			'changed' => array (
					'title' => 'Changed',
					'help' => 'The date the UIN was most recently saved.',
					'field' => array (
							'handler' => 'views_handler_field_date',
							'click sortable' => TRUE 
					),
					'sort' => array (
							'handler' => 'views_handler_sort_date' 
					),
					'filter' => array (
							'handler' => 'views_handler_filter_date' 
					) 
			),
			'extradata' => array (
					'title' => 'Extradata',
					'help' => 'A serialized array of additional data. For future use.',
					'field' => array (
							'handler' => 'views_handler_field',
							'click sortable' => FALSE 
					) 
			) 
	);
	
	return $data;
}