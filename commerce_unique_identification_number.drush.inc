<?php
function commerce_unique_identification_number_drush_command()
{
	$items = array();
	$items['commerce_uin_queue_run'] = array(
			'description' => "Pulls any outstanding tasks from the queue and creates outstanding uin's.",
			'callback' => 'commerce_uin_queue_run',
			'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL, 
			'aliases' => array('comm-uin-run'),
	);
	return $items;
}

function commerce_uin_queue_run()
{
	$queue = DrupalQueue::get( COMMERCE_UIN_QUEUE );
	while ($item = $queue->claimItem()) {
		commerce_unique_identification_number_worker($item);
		$queue->deleteItem($item);
	}
}

?>