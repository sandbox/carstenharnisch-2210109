<?php
/**
 * @file
 * The database schema for the uin entity.
 */

/**
 * Implements hook_schema().
 */
function commerce_unique_identification_number_schema() {
  $schema = array();

  $schema['commerce_unique_identification_number'] = array(
    'description' => 'The base table for UINs.',
    'fields' => array(
      'sid' => array(
        'description' => 'The primary identifier for a UIN, used internally only.',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'order_id' => array(
        'description' => 'The {users}.uid that this UIN belongs to.',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'key_owner_id' => array(
        'description' => 'The {commerce_product_order}.uid the UIN belongs to.',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'product_id' => array(
        'description' => 'The {commerce_product_order}.product the UIN belongs to.',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'code' => array(
        'description' => 'The unique, human-readable identifier for a UIN.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      	'default' => 'NOT_YET_SET'
      ),
      'type' => array(
        'description' => 'The {commerce_product_key_type}.type of this UIN. For future use.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => 'default',
      ),
      'activated' => array(
        'description' => 'Boolean indicating whether or not the UIN has been activated.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'revoked' => array(
        'description' => 'Boolean indicating whether or not the UIN has been revoked.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'language' => array(
        'description' => 'The {languages}.language of this UIN.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'uid' => array(
        'description' => 'The {users}.uid that created this key. Normally this is done within the checkout, so the uid of the buyer',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'status' => array(
        'description' => 'The status of this key.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the UIN key was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the UIN key was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'extradata' => array(
        'type' => 'blob',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
        'description' => 'A serialized array of additional data. For future use.',
      ),
    ),
    'primary key' => array('sid'),
    'indexes' => array(
      'type' => array('type'),
    ),
    'unique keys' => array(
      'code' => array('code'),
    ),
    'foreign keys' => array(
      'creator' => array(
        'table' => 'users',
        'columns' => array('uid' => 'uid'),
      ),
      'key_owner_id' => array(
        'table' => 'users',
        'columns' => array('uid' => 'uid'),
      ),
      'order_id' => array(
        'table' => 'commerce_order',
        'columns' => array('order_id' => 'order_id'),
      ),
    ),
  );

  return $schema;
}
